/* global describe, after, before, it */
'use strict'

const amqp = require('amqplib')

let _channel = null
let _conn = null
let app = null

describe('Tomas Connector Test', () => {
  before('init', () => {
    amqp.connect(process.env.BROKER)
      .then((conn) => {
        _conn = conn
        return conn.createChannel()
      }).then((channel) => {
        _channel = channel
      }).catch((err) => {
        console.log(err)
      })
  })

  after('close connection', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)
      app = require('../app')
      app.once('init', done)
    })
  })

  describe('#data', () => {
    it('should send data to third party client', function (done) {
      this.timeout(15000)

      let data = {
        // "defectId": "0b33cb16-b71a-11e9-a2a3-2a2ae2dbcce4",
        // "deviceId": "e4e48dcd-1352-4d41-8702-2c88fe342e3d",
        // "defectType": "AI.CROCODILECRACKING",
        // "severity": 3,
        // "imagery": "http://139.162.18.33/40ba8cb8-fd7c-4f81-b51b-79b5d7e8ded1/defects/0b33cb16-b71a-11e9-a2a3-2a2ae2dbcce4.png",
        // "change": "updated",
        // "last_seen": "2019-07-31T17:25:33",
        // "passed": 1,
        // "missed": 0,
        // "dateMoment": "31/07/2019 05:25 PM",
        // "detected": "2019-07-31T17:25:33",
        // "latitude": "-27.073011",
        // "longitude": "153.055969",
        // "classification": "Crocodile Cracking",
        // "AssetNumber": "A00550312"
        // 'pothole_id': '3a114b7c-3bcb-11e9-b210-d663bd873d93',
        // 'severity': 1,
        // 'type': 'repaired',
        // 'last_seen': '2019-03-01T08:23:52',
        // 'passed': 1,
        // 'missed': 0,
        // 'detected': '2019-03-01T08:23:52',
        // // 'detected': '20180701112354'
        // 'latitude': '-27.17729599999999834',
        // 'dateMoment': '01/07/2018 11:23 AM',
        // 'longitude': '152.97252299999999536',
        // 'classification': 'fixed',
        // 'AssetNumber': 'A00559595',
        // 'camera_id': "123abc",
        // 'imageURL': "https://abc.com/images/529d2fd7-e892-4cef-913a-7056339cde0d.jpg"

      //   {
      //     "defect_id": "1a8e3413-0a32-4170-b272-eed5213cff37", used to be “pothole_id”
      //     "device_id": "527a4209-95b4-4018-8af8-800b35b47c76", used to be “camera_id”
      //     "defect_type": "pothole", This should go through to the defect code field in TechOne (see notes on this below)
      //     "severity": "3",
      //     "imagery": "http://139.162.18.33/defects/1a8e3413-0a32-4170-b272-eed5213cff37.png", used to be “imageURL”
      //     "latitude": "-27.263197",
      //     "longitude": "152.957169",
      //     "change": "new", used to be “type”
      //     "detected": "20190624135226",
      //     "last_seen": "20190624135226",
      //     "passed": "1",
      //     "missed": "0"
      // }

        // "pothole_id": "5b2feeaf-3a41-44fb-a1a5-678ba86542fa",
        // "severity": 2,
        // "type": "new",
        // "last_seen": "2019-05-27T13:57:24",
        // "passed": 1,
        // "missed": 0,
        // "dateMoment": "27/05/2019 01:57 PM",
        // "detected": "2019-05-27T13:57:24",
        // "latitude": "-27.160206",
        // "longitude": "153.003403",
        // "classification": "pothole_small",
        // "AssetNumber": "A00558323"



        // "pothole_id": "3352a396-7bc0-11e9-8f9e-2a86e4085a59",
        // "severity": 1,
        // "type": "repaired",
        // "last_seen": "2019-05-20T13:54:43",
        // "passed": 1,
        // "missed": 0,
        // "dateMoment": "20/05/2019 01:54 PM",
        // "detected": "2019-05-20T13:54:43",
        // "latitude": "-27.183954",
        // "longitude": "153.026154",
        // "classification": "developing",
        // "AssetNumber": "A00547985",
        // "camera_id": "123abc",
        // "imageURL": "https://abc.com/images/529d2fd7-e892-4cef-913a-7056339cde0d.jpg"


      }

      _channel.sendToQueue('ip.tomas', Buffer.from(JSON.stringify(data)))
      setTimeout(done, 10000)
    })
  })
})
