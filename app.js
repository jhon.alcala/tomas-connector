'use strict'

const Reekoh = require('reekoh')
const plugin = new Reekoh.plugins.Connector()
/* eslint-disable new-cap */
const rkhLogger = new Reekoh.logger('tomas-connector')
const request = require('request-promise')
const get = require('lodash.get')
const xml2js = require('xml2js')
const builder = new xml2js.Builder()
const moment = require('moment')

let parseString = require('xml2js').parseString
let isEmpty = require('lodash.isempty')

let payloadTemplate
let doRead
let updatePothole
let result
let options

plugin.on('data', (data) => {
  let ts = plugin.processStart()
  let lat = get(data, 'latitude')
  let lng = get(data, 'longitude')
  let classification = get(data, 'classification')
  let dateMoment = get(data, 'dateMoment')
  let assetNumber = get(data, 'AssetNumber')
  let detectedDate = get(data, 'detected')
  let defectId = get(data, 'defectId') // before pothole_id
  let severity = get(data, 'severity') || 0
  let change = get(data, 'change') || '' // before type
  let lastSeen = get(data, 'last_seen')
  let passed = get(data, 'passed') || 0
  let missed = get(data, 'missed') || 0
  let defectType = get(data, 'defectType') // before defect_code
  let deviceId = get(data, 'deviceId') // before camera_id
  let imagery = get(data, 'imagery')

  if (classification !== 'Pothole' && classification !== 'Cracking' && classification !== 'Crocodile Cracking' && classification !== 'Shoving') {
    return plugin.logException(new Error(`Invalid Classification value ${classification}`))
  }

  if (isEmpty(change)) {
    plugin.processDone(ts)
    return plugin.logException(new Error('Invalid Data. Change cannot be empty'))
  }

  if (change !== 'new' && change !== 'updated' && change !== 'repaired' && change !== 'fixed') {
    plugin.processDone(ts)
    return plugin.logException(new Error('Invalid Change. Only "Change": new, updated, repaired or fixed'))
  }

  if (isEmpty(assetNumber)) {
    plugin.processDone(ts)
    return plugin.logException(new Error('Invalid Data. AssetNumber cannot be empty'))
  }

  if (change === 'new') {
    // creating of new potholes
    payloadTemplate = {
      's:Envelope': {
        '$': {
          'xmlns:s': 'http://www.w3.org/2003/05/soap-envelope'
        },
        's:Body': [
          {
            '$': {
              'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
              'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema'
            },
            'DefectInstance_DoCreate': [
              {
                '$': {
                  'xmlns': 'http://TechnologyOneCorp.com/T1.W1.Public/Services'
                },
                'Request': [
                  {
                    '$': {
                      'PerformSaveAttachments': 'true'
                    },
                    'Auth': [
                      {
                        '$': {
                          'UserId': `${plugin.config.username}`,
                          'Password': `${plugin.config.password}`,
                          'Config': `${plugin.config.config}`,
                          'FunctionName': '$W1.DEFINST.CREAT.WS'
                        }
                      }
                    ],
                    'DefectInstances': [
                      {
                        'DefectInstances': [
                          {
                            '$': {
                              'DefectTime': `${dateMoment}`,
                              'DefectDate': `${dateMoment}`,
                              'Source': 'IM',
                              'Status': 'I',
                              'State': 'Added',
                              'DefectCode': `${defectType}`,
                              'AssetNumber': `${assetNumber}`,
                              'RegisterName': 'MBRCOP',
                              'Stage': `R`
                            },
                            'Attachments': [
                              {
                                'Attachments': [
                                  {
                                    '$': {
                                      'AttLabel': 'Photo new',
                                      'AttFileName': `${imagery}`,
                                      'AttFileExt': 'URL',
                                      'AttTypeInd': 'U',
                                      'Action': 'I'
                                    }
                                  }
                                ]
                              }
                            ],
                            'Geographies': {
                              'Geographies': [
                                {
                                  '$': {
                                    'ElementType': '1',
                                    'Layer': 'Defect',
                                    'State': 'Added'
                                  },
                                  'Points': [
                                    {
                                      '$': {
                                        'Latitude': `${lat}`,
                                        'Longitude': `${lng}`
                                      }
                                    }
                                  ]
                                }
                              ]
                            },
                            'DefectCodeCustomFieldValues': [
                              {

                                'DefectCodeCustomFieldValues': [
                                  {
                                    '$': {
                                      'ValAlpha': `${defectId}`
                                    }
                                  }, {
                                    '$': {
                                      'ValNum': `${severity}`
                                    }
                                  }, {
                                    '$': {
                                      'ValAlpha': `${change}`
                                    }
                                  }, {
                                    '$': {
                                      'ValDateTime': `${lastSeen}`
                                    }
                                  }, {
                                    '$': {
                                      'ValNum': `${passed}`
                                    }
                                  }, {
                                    '$': {
                                      'ValNum': `${missed}`
                                    }
                                  }, {
                                    '$': {
                                      'ValDateTime': `${detectedDate}`
                                    }
                                  }, {
                                    '$': {
                                      'ValAlpha': `${classification}`
                                    }
                                  }, {
                                    '$': {
                                      'ValAlpha': `${deviceId}`
                                    }
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    }

    result = builder.buildObject(payloadTemplate)
    console.log('result------new', result)

    options = {
      method: 'POST',
      uri: plugin.config.url,
      headers: {
        'Content-Length': Buffer.byteLength(result),
        'SOAPAction': 'http://TechnologyOneCorp.com/T1.W1.Public/Services/DefectInstance_DoCreate',
        'Content-Type': 'text/xml',
        'Accept-Charset': 'utf-8',
        'cache-control': 'no-cache'
      },
      body: result,
      json: false
    }

    return request(options)
      .then((response) => {
        return parseString(response, (err, result) => {
          if (err) {
            console.log(err)
          }

          let handleError = result['soap:Envelope']['soap:Body'][0]['DefectInstance_DoCreateResponse'][0]['DefectInstance_DoCreateResult'][0]['Errors'][0]['$']['IsError']

          if (handleError === 'false') {
            plugin.processDone(ts)
            return plugin.log({
              title: 'Data Received - Tomas Connector',
              message: `Creating of ${classification}`,
              data: data,
              response: response
            })
          } else {
            return plugin.logException(new Error(response))
          }
        })
      })
      .catch((err) => {
        console.log(new Error(err))
        plugin.processDone(ts)
        return plugin.logException(new Error(err))
      })
  } else if (change === 'repaired' || change === 'updated' || change === 'fixed') {
    // updating or closing of potholes
    doRead = {
      's:Envelope': {
        '$': {
          'xmlns:s': 'http://schemas.xmlsoap.org/soap/envelope/'
        },
        's:Header': [
          ''
        ],
        's:Body': [
          {
            '$': {
              'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
              'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema'
            },
            'DefectInstance_DoRead': [
              {
                '$': {
                  'xmlns': 'http://TechnologyOneCorp.com/T1.W1.Public/Services'
                },
                'Request': [
                  {
                    '$': {
                      'RegName': 'MBRCOP',
                      'AssetNumberi': `${assetNumber}`,
                      'PerformReadAttachments': 'true'
                    },
                    'Auth': [
                      {
                        '$': {
                          'UserId': `${plugin.config.username}`,
                          'Password': `${plugin.config.password}`,
                          'Config': `${plugin.config.config}`,
                          'FunctionName': '$W1.DEFINST.READ.WS'
                        }
                      }
                    ],
                    'Criteria': [
                      {
                        'critSpec': [
                          {
                            '$': {
                              'OperatorCode': `=`,
                              'PropertyName': `DefectCodeCustomFieldValue1`,
                              'Value': `${defectId}`
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    }

    result = builder.buildObject(doRead)
    console.log('result do read------------', result)

    options = {
      method: 'POST',
      uri: plugin.config.url,
      headers: {
        'Content-Length': Buffer.byteLength(result),
        'SOAPAction': `http://TechnologyOneCorp.com/T1.W1.Public/Services/DefectInstance_DoRead`,
        'Content-Type': 'text/xml',
        'Accept-Charset': 'utf-8',
        'cache-control': 'no-cache'
      },
      body: result,
      json: false
    }
    return request(options)
      .then((response) => {
        const opts = {
          mergeAttrs: true,
          attrNameProcessors: [str => '@' + str]
        }

        xml2js.parseString(response, opts, (err, res) => {
          if (err) {
            return plugin.logException(new Error(err))
          }
          let defectUniqueId = res['soap:Envelope']['soap:Body'][0]['DefectInstance_DoReadResponse'][0]['DefectInstance_DoReadResult'][0]['DefectInstances'][0]['DefectInstances'][0]['@DefectUniqueId'][0]
          console.log('defectUniqueID---------->', defectUniqueId)

          if (isEmpty(defectUniqueId)) {
            return plugin.logException(new Error('Invalid DefectUniqueId. DefectUniqueId could not be empty.'))
          }
          let defectTime = moment(detectedDate, 'YYYY-MM-DD[T]HH:mm:ss').format('HH:mm:ss A')
          let defectDate = moment(detectedDate, 'YYYY-MM-DD[T]HH:mm:ss').format('DD-MMM-YYYY')

          if (change === 'repaired' || change === 'fixed') {
            console.log('closing of potholes')
            let complTime = moment(lastSeen, 'YYYY-MM-DD[T]HH:mm:ss').format('HH:mm:ss A')
            let complDate = moment(lastSeen, 'YYYY-MM-DD[T]HH:mm:ss').format('DD/MM/YYYY')

            updatePothole = {
              's:Envelope': {
                '$': {
                  'xmlns:s': 'http://schemas.xmlsoap.org/soap/envelope/'
                },
                's:Body': [
                  {
                    '$': {
                      'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                      'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema'
                    },
                    'DefectInstance_DoUpdate': [
                      {
                        '$': {
                          'xmlns': 'http://TechnologyOneCorp.com/T1.W1.Public/Services'
                        },
                        'Request': [
                          {
                            '$': {
                              'PerformSaveAttachments': 'true'
                            },
                            'Auth': [
                              {
                                '$': {
                                  'UserId': `${plugin.config.username}`,
                                  'Password': `${plugin.config.password}`,
                                  'Config': `${plugin.config.config}`,
                                  'FunctionName': '$W1.DEFINST.UPDAT.WS'
                                }
                              }
                            ],
                            'DefectInstances': [
                              {
                                'DefectInstances': [
                                  {
                                    '$': {
                                      'ComplTime': `${complTime}`,
                                      'ComplDate': `${complDate}`,
                                      'DefectTime': `${defectTime}`,
                                      'DefectDate': `${defectDate}`,
                                      'Source': 'IM',
                                      'Status': `I`,
                                      'State': 'Modified',
                                      'DefectUniqueId': `${defectUniqueId}`,
                                      'DefectCode': `${defectType}`,
                                      'AssetNumber': `${assetNumber}`,
                                      'RegisterName': 'MBRCOP',
                                      'Stage': `W`
                                    },
                                    'Attachments': [
                                      {
                                        'Attachments': [
                                          {
                                            '$': {
                                              'AttLabel': 'Photo closed',
                                              'AttFileName': `${imagery}`,
                                              'AttFileExt': 'URL',
                                              'AttTypeInd': 'U',
                                              'Action': 'I'
                                            }
                                          }
                                        ]
                                      }
                                    ],
                                    'CustomFieldValues': { // CustomFieldValues element attribute of the defect instance. Note: use of array to create subelements.
                                    },
                                    'DefectCodeCustomFieldValues': [
                                      {

                                        'DefectCodeCustomFieldValues': [
                                          {
                                            '$': {
                                              'ValAlpha': `${defectId}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${severity}`
                                            }
                                          }, {
                                            '$': {
                                              'ValAlpha': `${change}`
                                            }
                                          }, {
                                            '$': {
                                              'ValDateTime': `${lastSeen}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${passed}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${missed}`
                                            }
                                          }, {
                                            '$': {
                                              'ValDateTime': `${detectedDate}`
                                            }
                                          }, {
                                            '$': {
                                              'ValAlpha': `${classification}`
                                            }
                                          },
                                          {
                                            '$': {
                                              'ValAlpha': `${deviceId}`
                                            }
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            }

            let resultUpdate = builder.buildObject(updatePothole)

            let optionsUpdates = {
              method: 'POST',
              uri: plugin.config.url,
              headers: {
                'Content-Length': Buffer.byteLength(resultUpdate),
                'SOAPAction': `http://TechnologyOneCorp.com/T1.W1.Public/Services/DefectInstance_DoUpdate`,
                'Content-Type': 'text/xml',
                'Accept-Charset': 'utf-8',
                'cache-control': 'no-cache'
              },
              body: resultUpdate,
              json: false
            }

            return request(optionsUpdates)
              .then((response) => {
                return parseString(response, (err, result) => {
                  if (err) {
                    console.log(err)
                  }

                  let handleError = result['soap:Envelope']['soap:Body'][0]['DefectInstance_DoUpdateResponse'][0]['DefectInstance_DoUpdateResult'][0]['Errors'][0]['$']['IsError']

                  if (handleError === 'false') {
                    plugin.processDone(ts)
                    return plugin.log({
                      title: 'Data Received - Tomas Connector',
                      message: `Closing of ${classification}`,
                      data: data,
                      response: response
                    })
                  } else {
                    return plugin.logException(new Error(response))
                  }
                })
              })
              .catch((err) => {
                console.log('catch--------------------------', err)
                plugin.processDone(ts)
                return plugin.logException(new Error(err))
              })
          } else if (change === 'updated') {
            console.log('updating of Potholes')
            updatePothole = {
              's:Envelope': {
                '$': {
                  'xmlns:s': 'http://schemas.xmlsoap.org/soap/envelope/'
                },
                's:Body': [
                  {
                    '$': {
                      'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                      'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema'
                    },
                    'DefectInstance_DoUpdate': [
                      {
                        '$': {
                          'xmlns': 'http://TechnologyOneCorp.com/T1.W1.Public/Services'
                        },
                        'Request': [
                          {
                            '$': {
                              'PerformSaveAttachments': 'true'
                            },
                            'Auth': [
                              {
                                '$': {
                                  'UserId': `${plugin.config.username}`,
                                  'Password': `${plugin.config.password}`,
                                  'Config': `${plugin.config.config}`,
                                  'FunctionName': '$W1.DEFINST.UPDAT.WS'
                                }
                              }
                            ],
                            'DefectInstances': [
                              {
                                'DefectInstances': [
                                  {
                                    '$': {
                                      'DefectTime': `${defectTime}`,
                                      'DefectDate': `${defectDate}`,
                                      'Source': `IM`,
                                      'Status': `A`,
                                      'State': 'Modified',
                                      'DefectUniqueId': `${defectUniqueId}`,
                                      'DefectCode': `${defectType}`,
                                      'AssetNumber': `${assetNumber}`,
                                      'RegisterName': `MBRCOP`,
                                      'Stage': `R`
                                    },
                                    'Attachments': [
                                      {
                                        'Attachments': [
                                          {
                                            '$': {
                                              'AttLabel': 'Photo updated',
                                              'AttFileName': `${imagery}`,
                                              'AttFileExt': 'URL',
                                              'AttTypeInd': 'U',
                                              'Action': 'I'
                                            }
                                          }
                                        ]
                                      }
                                    ],
                                    'CustomFieldValues': { // CustomFieldValues element attribute of the defect instance. Note: use of array to create subelements.
                                    },
                                    'DefectCodeCustomFieldValues': [
                                      {

                                        'DefectCodeCustomFieldValues': [
                                          {
                                            '$': {
                                              'ValAlpha': `${defectId}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${severity}`
                                            }
                                          }, {
                                            '$': {
                                              'ValAlpha': `${change}`
                                            }
                                          }, {
                                            '$': {
                                              'ValDateTime': `${lastSeen}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${passed}`
                                            }
                                          }, {
                                            '$': {
                                              'ValNum': `${missed}`
                                            }
                                          }, {
                                            '$': {
                                              'ValDateTime': `${detectedDate}`
                                            }
                                          }, {
                                            '$': {
                                              'ValAlpha': `${classification}`
                                            }
                                          }, {
                                            '$': {
                                              'ValAlpha': `${deviceId}`
                                            }
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            }
          }

          let resultUpdate = builder.buildObject(updatePothole)
          console.log('resultUpdate', resultUpdate)

          let optionsUpdates = {
            method: 'POST',
            uri: plugin.config.url,
            headers: {
              'Content-Length': Buffer.byteLength(resultUpdate),
              'SOAPAction': `http://TechnologyOneCorp.com/T1.W1.Public/Services/DefectInstance_DoUpdate`,
              'Content-Type': 'text/xml',
              'Accept-Charset': 'utf-8',
              'cache-control': 'no-cache'
            },
            body: resultUpdate,
            json: false
          }

          return request(optionsUpdates)
            .then((response) => {
              return parseString(response, (err, result) => {
                if (err) {
                  console.log(err)
                }

                let handleError = result['soap:Envelope']['soap:Body'][0]['DefectInstance_DoUpdateResponse'][0]['DefectInstance_DoUpdateResult'][0]['Errors'][0]['$']['IsError']
                if (handleError === 'false') {
                  plugin.processDone(ts)
                  return plugin.log({
                    title: 'Data Received - Tomas Connector',
                    message: `Updating of ${classification}`,
                    data: data,
                    response: response
                  })
                } else {
                  return plugin.logException(new Error(response))
                }
              })
            })
            .catch((err) => {
              console.log('catch--------------------------', err)
              plugin.processDone(ts)
              return plugin.logException(new Error('Invalid Data. Error in Updating of Potholes.'))
            })
        })
      })
      .catch((err) => {
        console.log('catch-------------------------- response', err)
        plugin.processDone(ts)
        return plugin.logException(new Error('Invalid Data. Error in Updating of Potholes.'))
      })
  } else {
    return plugin.logException(new Error('Invalid Data Change. Change should be new, updated or repaired.'))
  }
})

plugin.once('ready', () => {
  plugin.log('Tomas Connector has been initialized.')
  rkhLogger.info('Tomas Connector has been initialized.')
  plugin.emit('init')
})

module.exports = plugin
